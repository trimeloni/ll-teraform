#!/usr/bin/env bash

# Some standard first time ubuntu items
apt-get update
#apt-get -y dist-upgrade
#apt-get install -y git
apt-get install -y unzip


#
# Update to get the latest git version instead of the Trusty Package version
#
add-apt-repository ppa:git-core/ppa
apt-get update
apt-get install -y git

#
# Install Terraform
#

#
mkdir /home/vagrant/terraform
cd /home/vagrant/terraform

# Download Terraform. URI: https://www.terraform.io/downloads.html
curl -O https://releases.hashicorp.com/terraform/0.8.2/terraform_0.8.2_linux_amd64.zip

# Unzip and install
unzip terraform_0.8.2_linux_amd64.zip

# Update 
echo '
# Terraform Paths.
export PATH=/home/vagrant/terraform:$PATH
' >>/home/vagrant/.bash_profile

source /home/vagrant/.bash_profile



#
# Install Packer
#

mkdir /home/vagrant/packer
cd /home/vagrant/packer

# Download Packer. URI: https://www.packer.io/downloads.html
curl -O https://releases.hashicorp.com/packer/0.12.1/packer_0.12.1_linux_amd64.zip
# Unzip and install
unzip packer_0.12.1_linux_amd64.zip

echo '
# Packer Paths.
export PATH=/home/vagrant/packer/:$PATH
' >>/home/vagrant/.bash_profile

source /home/vagrant/.bash_profile

  
         

# All Set, give the user some information
#echo 'Goto: http://33.33.33.110:8080'
#echo 'To get Git integration, need to fire up ngrok or setup firewall forwarding and vm host forwarding...'
#echo 'The value of /var/lib/jenkins/secrets/initialAdminPassword is: ' && cat /var/lib/jenkins/secrets/initialAdminPassword








