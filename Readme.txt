
#
# Links/Resources
#
Tutorial Link:
  https://www.terraform.io/intro/
 
L&L Links:
  Vagrant Files:
  
  
Other Links:
  Original Terraform/Packer: https://gist.github.com/Adron/90863e51c8c5c0ad2049890bcd8abbfb
  Terraform Download: https://www.terraform.io/downloads.html
  Packer Download: https://www.packer.io/downloads.html
  
  An Introduction to Terraform: https://blog.gruntwork.io/an-introduction-to-terraform-f17df9c6d180#.602qddph7
  A Practical Introduction to Terraform: https://adamcod.es/2015/10/22/a-pracitcal-introduction-to-terraform-tutorial.html
    Ansible
    Managing Digital Ocean Droplets
    Provisioning VMs with a PHP Stack
    Managing CRON entries
    Managing Firewall rules
    Creating new nginx virtual hosts
    Automated deployments on git push to master
    
  
  
  

#
# L&L 
#

Git pull Down
  vagrant up

#
# Terraform commands
#
terraform plan
terraform apply
terraform show
terraform plan -destroy
terraform destroy

